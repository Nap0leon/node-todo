// Javascript to control the behavior of your todo list
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

// Connect to the MongoDB database
mongoose.connect('');

// Create a schema - This is like a blueprint for our data
var todoSchema = new mongoose.Schema({
    item: String
});

// Create a model type on todo, based on todoSchema
var Todo = mongoose.model('Todo', todoSchema);

// Mongo DB test for itemOne
// var itemOne = Todo({item: 'Code it bro'}).save(function(err){
//     if(err) throw err;
//     console.log('item saved');
// });


// var data = [{item: 'Get milk'}, {item: 'Walk the dog'}, {item: 'Code some stuff'}]
var urlencodedParser = bodyParser.urlencoded({extended : false});

module.exports = function(app){
    app.get('/todo', function(req, res) {
        // get data from mongodb and pass it to view
        // Find method can find all item {} or a specific one {item: 'buy flower'}
        Todo.find({}, function(err, data){
            if (err) throw err;
            res.render('todo', {todos: data});
        });
        
    });

    app.post('/todo', urlencodedParser, function(req, res) {
        // get data from the view and add it to mongodb
        // save it to the data base and fire function(err,data)
        var newTodo = Todo(req.body).save(function(err,data){
            if (err) throw err;
            res.json(data);
        });
    });

    app.delete('/todo/:item', function(req, res) {
        // delete the requested item from mongodb
        // replace the - with a space. 
        Todo.find({item: req.params.item.replace(/\-/g, " ")}).remove(function(err, data) {
            if (err) throw err;
            res.json(data);
        });
    });
};
